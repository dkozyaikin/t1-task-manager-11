package ru.t1.dkozyaikin.tm.api;

import ru.t1.dkozyaikin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project add(Project project);

    void clear();

}
