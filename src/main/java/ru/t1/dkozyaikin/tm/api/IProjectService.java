package ru.t1.dkozyaikin.tm.api;

import ru.t1.dkozyaikin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
