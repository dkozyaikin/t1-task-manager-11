package ru.t1.dkozyaikin.tm.api;

import ru.t1.dkozyaikin.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void displayTask(Task task);

    void displayTaskById();

    void displayTaskByIndex();

    void displayTasks();

    void updateTaskById();

    void updateTaskByIndex();

}
