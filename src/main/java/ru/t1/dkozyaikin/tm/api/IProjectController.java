package ru.t1.dkozyaikin.tm.api;

import ru.t1.dkozyaikin.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void displayProject(Project project);

    void displayProjectById();

    void displayProjectByIndex();

    void displayProjects();

    void updateProjectById();

    void updateProjectByIndex();

}
