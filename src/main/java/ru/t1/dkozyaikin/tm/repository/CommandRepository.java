package ru.t1.dkozyaikin.tm.repository;

import ru.t1.dkozyaikin.tm.api.ICommandRepository;
import ru.t1.dkozyaikin.tm.model.Command;

import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.*;
import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.ARG_INFO;
import static ru.t1.dkozyaikin.tm.constant.TerminalConst.*;

public final class CommandRepository implements ICommandRepository {

    private final static Command VERSION = new Command(
            CMD_VERSION, ARG_VERSION,
            "Display app version."
    );

    private final static Command ABOUT = new Command(
            CMD_ABOUT, ARG_ABOUT,
            "Display developer info."
    );

    private final static Command HELP = new Command(
            CMD_HELP, ARG_HELP,
            "Display app commands."
    );

    private final static Command EXIT = new Command(
            CMD_EXIT, null,
            "Close app."
    );

    private final static Command INFO = new Command(
            CMD_INFO, ARG_INFO,
            "Display system info."
    );

    private final static Command PROJECT_LIST = new Command(
            CMD_PROJECT_LIST, null,
            "Display project list."
    );

    private final static Command PROJECT_CLEAR = new Command(
            CMD_PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private final static Command PROJECT_CREATE = new Command(
            CMD_PROJECT_CREATE, null,
            "Create new project."
    );

    private final static Command PROJECT_DISPLAY_BY_ID = new Command(
            CMD_PROJECT_DISPLAY_BY_ID, null,
            "Display project by id."
    );

    private final static Command PROJECT_DISPLAY_BY_INDEX = new Command(
            CMD_PROJECT_DISPLAY_BY_INDEX, null,
            "Display project by index."
    );

    private final static Command PROJECT_UPDATE_BY_ID = new Command(
            CMD_PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    private final static Command PROJECT_UPDATE_BY_INDEX = new Command(
            CMD_PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    private final static Command PROJECT_REMOVE_BY_ID = new Command(
            CMD_PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    private final static Command PROJECT_REMOVE_BY_INDEX = new Command(
            CMD_PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    private final static Command TASK_LIST = new Command(
            CMD_TASK_LIST, null,
            "Display task list."
    );

    private final static Command TASK_CLEAR = new Command(
            CMD_TASK_CLEAR, null,
            "Remove all tasks."
    );

    private final static Command TASK_CREATE = new Command(
            CMD_TASK_CREATE, null,
            "Create new task."
    );

    private final static Command TASK_DISPLAY_BY_ID = new Command(
            CMD_TASK_DISPLAY_BY_ID, null,
            "Display task by id."
    );

    private final static Command TASK_DISPLAY_BY_INDEX = new Command(
            CMD_TASK_DISPLAY_BY_INDEX, null,
            "Display task by index."
    );

    private final static Command TASK_UPDATE_BY_ID = new Command(
            CMD_TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    private final static Command TASK_UPDATE_BY_INDEX = new Command(
            CMD_TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    private final static Command TASK_REMOVE_BY_ID = new Command(
            CMD_TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    private final static Command TASK_REMOVE_BY_INDEX = new Command(
            CMD_TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, EXIT, HELP, ABOUT, VERSION,
            PROJECT_LIST, PROJECT_CLEAR, PROJECT_CREATE,
            PROJECT_DISPLAY_BY_ID, PROJECT_DISPLAY_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            TASK_LIST, TASK_CLEAR, TASK_CREATE,
            TASK_DISPLAY_BY_ID, TASK_DISPLAY_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}

