package ru.t1.dkozyaikin.tm.controller;

import ru.t1.dkozyaikin.tm.api.IProjectController;
import ru.t1.dkozyaikin.tm.api.IProjectService;
import ru.t1.dkozyaikin.tm.model.Project;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECTS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[DONE]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[DONE]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[DONE]");
    }

    @Override
    public void displayProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void displayProjectById() {
        System.out.println("[DISPLAY PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        displayProject(project);
        System.out.println("[DONE]");
    }

    @Override
    public void displayProjectByIndex() {
        System.out.println("[DISPLAY PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        displayProject(project);
        System.out.println("[DONE]");
    }

    @Override
    public void displayProjects() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[DONE]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateById(id, name, description);
        if (project == null) System.out.println("[FAIL]");
        System.out.println("[DONE]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) System.out.println("[FAIL]");
        System.out.println("[DONE]");
    }

}
